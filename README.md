## circles-task

Simple Test

Using the suggested API Endpoint

On Init => Retrieve data from the endpoint 

On Success => Show the Layout

The container must be 400x200 centred inside the page, but if we resize it, the test should still work. 

The colour of the ball must be random taken from the API, using the “hex” current colour.


1. POSITION 1 (BALL IN TOP-LEFT CORNER OF THE BOX AND COLOR #1)

On Click => call the endpoint, retrieve the next “hex” random color then move animating the ball to the opposite corner clockwise.


2. POSITION 2 (BALL IN TOP-RIGHT CORNER OF THE BOX AND COLOR #2)

On Click => call the endpoint, retrieve the next “hex” random color then move animating the ball to the opposite corner clockwise.


3. POSITION 3 (BALL IN TOP-RIGHT CORNER OF THE BOX AND COLOR #3)

On Click => call the endpoint, retrieve the next “hex” random color then move animating the ball to the opposite corner clockwise.


4. POSITION 4 (BALL IN TOP-RIGHT CORNER OF THE BOX AND COLOR #4)

On Click => return to first state.


You can choose your favourite JS framework, SCSS is preferable over simple CSS, use minimal HTML.

Clean code with syntax check is appreciated.

if you manage, minify the CSS and JS using a build tool (gulp, grunt, ngCli), in case provide a minimum of documentation.


API_ENDPOINT: //colr.org/json/color/random


## to run the app:
1. clone repository
2. run npm install
3. run ng serve or npm start