import { CircleDataService } from './circle-data.service';
import { Component, OnInit } from '@angular/core';

const nextPositions = {
  'top-left': 'top-right',
  'top-right': 'bottom-right',
  'bottom-right': 'bottom-left',
  'bottom-left': 'top-left'
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  public layoutVisible = false;
  public activeCirclePosition = 'top-left';
  public activeCircleColor: string;

  constructor(private cds: CircleDataService) { }

  ngOnInit() {
    this.cds.getColor().then(color => {
      this.layoutVisible = true;
      this.activeCircleColor = `#${color}`;
    });
  }

  public setNextPosition() {
    this.cds.getColor().then(color => {
      this.activeCircleColor = `#${color}`;
      this.activeCirclePosition = nextPositions[this.activeCirclePosition];
    });
  }
}
