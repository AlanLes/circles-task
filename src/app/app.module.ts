import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from  '@angular/common/http'
import { NgModule } from '@angular/core';
import { CircleDataService } from './circle-data.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [CircleDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
