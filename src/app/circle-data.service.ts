import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class CircleDataService {

  constructor(private http: HttpClient) { }

  public getColor(): Promise<string> {
    return this.http.get('http://www.colr.org/json/color/random')
      .toPromise()
      .then(res => res['colors'][0].hex);
  }
}
